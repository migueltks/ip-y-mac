package com.ks.validacion;

import java.net.*;
import java.util.Enumeration;

/**
 * Hello world!
 */
public class App
{
    static String VMstrIP;
    static String VMstrMac;
    static String VMstrPCName;

    public static void main(String[] args)
    {

        try
        {
            System.out.println("OS: " + System.getProperty("os.name").toUpperCase());
            System.out.println("IP: " + getIP());
            System.out.println("Mac: " + getMac());
            System.out.println("Name: " + getPCName());

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static String getIP()
    {
        String os = System.getProperty("os.name").toUpperCase();

        try
        {
            if (!os.contains("WINDOWS"))
            {
                int contador = 0;
                NetworkInterface ni;
                do
                {
                    ni = NetworkInterface.getByName("eth" + contador);
                    contador += 1;
                } while (ni == null);

                Enumeration<InetAddress> ias = ni.getInetAddresses();

                InetAddress iaddress;
                do
                {
                    iaddress = ias.nextElement();
                } while (!(iaddress instanceof Inet4Address));

                return iaddress.getHostAddress();
            }
            else
            {
                InetAddress ip = InetAddress.getLocalHost();
                return ip.getHostAddress();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    public static String getMac()
    {
        try
        {
            NetworkInterface network;

            if (System.getProperty("os.name").toUpperCase().contains("WINDOWS"))
            {
                InetAddress ip = InetAddress.getLocalHost();
                network = NetworkInterface.getByInetAddress(ip);
            }
            else
            {
                int contador = 0;
                do
                {
                    network = NetworkInterface.getByName("eth" + contador);
                    contador += 1;
                } while (network == null);
            }

            byte[] mac = network.getHardwareAddress();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++)
            {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            VMstrMac = sb.toString();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            VMstrMac = "ERROR";
        }
        return VMstrMac.toUpperCase();
    }

    public static String getPCName()
    {
        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            VMstrPCName = addr.getHostName();
            if (VMstrPCName.length() > 14)
            {
                VMstrPCName = VMstrPCName.substring(0, 14);
            }
        }
        catch (Exception ex)
        {
            VMstrPCName = "ERROR";
        }
        return VMstrPCName;
    }

}
